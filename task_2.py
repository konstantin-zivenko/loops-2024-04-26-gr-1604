# У реченні “Hello world” замінити всі літери “o” на “a”, а літери “l” на “e”.

string = "Hello world!"

new_string = ""
for symbol in string:
    if symbol == "o":
        new_string += "a"
    elif symbol == "l":
        new_string += "e"
    else:
        new_string += symbol

print(new_string)

